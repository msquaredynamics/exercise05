# Exercise05

Evolution of Exercise04 that uses the LiveData and ViewModel components.
Only the Portrait mode is handled.

The RecyclerView used in this app demonstrates:

- How to implement drag and swipe-to-remove capabilities together with ViewModel and LiveData
- How to efficiently compute changes into the dataset using the DiffUtil class provided by the support library.