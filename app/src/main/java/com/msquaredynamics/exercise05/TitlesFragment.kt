package com.msquaredynamics.exercise05

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.*
import android.widget.TextView
import com.msquaredynamics.exercise05.utils.BasicItemTouchHelperCallback
import com.msquaredynamics.exercise05.viewmodels.Article
import com.msquaredynamics.exercise05.viewmodels.ArticleViewModel
import kotlinx.android.synthetic.main.fragment_titles.*
import kotlinx.android.synthetic.main.listitem_fragment_titles.view.*


class TitlesFragment : Fragment() {

    private lateinit var viewLayoutManager: LinearLayoutManager
    private lateinit var viewAdapter: TitlesListAdapter
    private lateinit var itemTouchHelper: ItemTouchHelper

    private lateinit var articleViewModel: ArticleViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.run {
            articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewLayoutManager = LinearLayoutManager(context)


        viewAdapter = TitlesListAdapter(object: TitlesListAdapter.OnItemEventListener<Article> {
            override fun onClick(position: Int, article: Article) {
                articleViewModel.selectArticle(article.id)
            }

            override fun onDragIconPressed(position: Int) {
                itemTouchHelper.startDrag(recyclerview_fragmentTitles_titlesList.findViewHolderForAdapterPosition(position)!!)
            }


            override fun isSwipeable(article: Article): Boolean {
                // Does not allow swiping out the same article which is currently selected
                return articleViewModel.getSelectedArticleId().value != article.id
            }


            override fun onDismiss(position: Int, element: Article) {
                articleViewModel.deleteArticle(element.id)
            }

            override fun onMove(from: Int, to: Int) {
                articleViewModel.swapArticles(from, to)
            }
        })


        recyclerview_fragmentTitles_titlesList.apply recyclerView@{
            setHasFixedSize(true)
            layoutManager = viewLayoutManager
            adapter = viewAdapter

            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL)) // Add divider bars between items

            /** Handles swipe and drag events */
            itemTouchHelper = ItemTouchHelper(BasicItemTouchHelperCallback(viewAdapter, false, true)).apply {
                attachToRecyclerView(this@recyclerView)
            }
        }


        // Every time an article is removed or dragged to a different position, the ArticleViewModel will update the
        // articles list accordingly. Here we register an observer for the articles list so that when the latter gets
        // updated we can submit the updated list to the RecyclerView.
        articleViewModel.getArticles().observe(this, Observer {
            it?.let {
                viewAdapter.submitList(it)
            }
        })
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_titles, container, false)
    }

}



/**
 * Adapter class for the RecyclerView inside the TitlesFragment.
 * This class is the bridge between the dataset (articles) and its representation inside the RecyclerView.
 * When the user clicks on a Title, the OnItemEventListener@onClick callback is invoked, passing the position inside
 * the adapter of the item that was clicked.
 * When the user presses the drag icon, the OnItemEventListener@onDragIconPressed is invoked, passing the position inside
 * the adapter of the item.
 */
class TitlesListAdapter(
    val itemEventListener: OnItemEventListener<Article>) : RecyclerView.Adapter<TitlesListAdapter.TitlesViewHolder>(),
                                                       BasicItemTouchHelperCallback.ItemTouchHelperAdapter
{

    private lateinit var dataset: List<Article>

    /**
     * Interface used to communicate click, drag and swipe events up in the chain
     */
    interface OnItemEventListener<T> {
        /**
         * Notify when an item in the list is clicked.
         * @param position The position, in the adapter, of the item that was clicked
         * @param element The element that was clicked
         */
        fun onClick(position: Int, element: T)


        /**
         * Notify when the user presses the drag icon.
         * @param position The position, in the adapter, of the item
         */
        fun onDragIconPressed(position: Int)


        /**
         * Ask if an element can be swiped by the user
         * @param element The element the user wants to swipe
         * @return True if the element can be swiped, false otherwise
         */
        fun isSwipeable(element: T): Boolean


        /**
         * Notify when the user swipes an element of the list
         */
        fun onDismiss(position: Int, element: T)


        /** Notify that an item has been moved */
        fun onMove(from: Int, to: Int)
    }


    /**
     * Method called to submit a new list of articles to this adapter. This pattern is commonly used with RecyclerView
     * that handle LiveData<T> elements. Everytime the payload of the LiveData object changes, a new updated list is
     * submitted to the Adapter.
     * However, it would be inefficient to redraw the entire list every time, since often very few elements in the list
     * change for each submit. The DiffUtil class provides helper utilities that allow to handle this case very efficiently.
     * Basically, it compares the old list with the new list and dispatches updates only for those elements that really
     * changed.
     */
    fun submitList(newArticles: List<Article>) {
        if (!::dataset.isInitialized) {
            dataset = newArticles
            notifyItemRangeInserted(0, dataset.size)
        } else {
            val result = DiffUtil.calculateDiff(object: DiffUtil.Callback() {
                override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    // Checks internal fields equality.
                    return dataset[oldItemPosition] == newArticles[newItemPosition]
                }

                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    return dataset[oldItemPosition].id == newArticles[newItemPosition].id
                }

                override fun getNewListSize(): Int {
                    return newArticles.size
                }

                override fun getOldListSize(): Int {
                    return dataset.size
                }
            }, true)

            dataset = newArticles
            result.dispatchUpdatesTo(this)
        }
    }



    override fun getItemCount() = if (!::dataset.isInitialized) 0 else dataset.size



    /**
     * Called when a ViewHolder must be bound to an item in the dataset. In this case we simply set the text field
     * of the TextView to the item's title
     */
    override fun onBindViewHolder(viewHolder: TitlesViewHolder, position: Int) {
        viewHolder.title.text = dataset[position].title
    }



    /**
     * Called when a new ViewHolder object is created. We need to return the layout associated to this ViewHolder.
     * In this case it is the listitem_fragment_titles layout.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TitlesViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.listitem_fragment_titles, parent, false)
        return TitlesViewHolder(view)
    }



    /**
     * Custom ViewHolder class. Declared as "inner" so it can access the "itemEventListener" variable from the outer class
     */
    inner class TitlesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.textView_listitemFragmentTitles_title

        /**
         * We need to create an OnClickListener for the TextView and a OnTouchListener for the dragIcon.
         * The dragIcon listener must fire as soon as the user presses the icon, so we cannot use a normal "onClick"
         * event.
         */
        init {
            title.setOnClickListener {
                itemEventListener.onClick(adapterPosition, dataset[adapterPosition])
            }

            view.imageview_all_dragicon.setOnTouchListener { _, event ->
                if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                    itemEventListener.onDragIconPressed(adapterPosition)
                    return@setOnTouchListener false
                }
                false
            }
        }
    }




    /************************************************************************************
     *
     *  Override methods from BasicItemTouchHelperCallback.ItemTouchHelperAdapter
     *
     ***********************************************************************************/

    override fun onItemDismiss(position: Int) {
        itemEventListener.onDismiss(position, dataset[position])
    }


    override fun onItemMove(from: Int, to: Int) {
        itemEventListener.onMove(from, to)
    }

    override fun isSwipeable(position: Int): Boolean {
        return itemEventListener.isSwipeable(dataset[position])
    }
}


