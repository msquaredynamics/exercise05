package com.msquaredynamics.exercise05.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel


data class Article(var id: Int, var title: String, var description: String)

class ArticleViewModel : ViewModel() {
    private lateinit var articles: MutableLiveData<List<Article>>
    private lateinit var selectedArticleId: MutableLiveData<Int>


    /**
     * @return articles The list of the articles, wrapped in a LiveData object.
     */
    fun getArticles(): LiveData<List<Article>> {
        if (!::articles.isInitialized) {
            articles = MutableLiveData()
            loadArticles()
        }
        return articles
    }



    private fun loadArticles() {
        articles.value = MutableList<Article>(20) {
            Article(it, "Title $it", "Description of title $it")
        }
    }


    /**
     * Return the ID of the selected article
     * @return selectedArticleId LiveData object containing the selected article ID
     */
    fun getSelectedArticleId(): LiveData<Int> {
        if (!::selectedArticleId.isInitialized) {
            selectedArticleId = MutableLiveData()
            selectedArticleId.value = -1
        }

        return selectedArticleId
    }


    /**
     * Set an article as "selected"
     * @param articleId ID of the article to select
     */
    fun selectArticle(articleId: Int) {
        if (!::selectedArticleId.isInitialized) {
            selectedArticleId = MutableLiveData()
        }
        selectedArticleId.value = articleId
    }


    /**
     * Deletes an article
     * @param articleId ID of the article to delete
     */
    fun deleteArticle(articleId: Int) {
        val newList: MutableList<Article> = articles.value!!.toMutableList()
        newList.removeIf { it.id == articleId }
        articles.value = newList
    }


    /**
     * Swap the position of two articles
     * @param from Position of the first article
     * @param to   Position of the second article
     */
    fun swapArticles(from: Int, to: Int) {
        val newList: MutableList<Article> = articles.value!!.toMutableList()
        val tmp = newList[to]
        newList[to] = newList[from]
        newList[from] = tmp
        articles.value = newList
    }

}