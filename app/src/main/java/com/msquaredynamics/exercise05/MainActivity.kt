package com.msquaredynamics.exercise05


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.msquaredynamics.exercise05.viewmodels.ArticleViewModel


class MainActivity : AppCompatActivity() {
    private lateinit var articleViewModel: ArticleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)


        // The first time the activity is created, no fragment is drawn.
        // Therefore we add the "TitlesFragment"
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragmentContainer_activity_main_portrait, TitlesFragment(), "titlesFragment")
                .commit()

        }


        // Observe the selected article. When an article gets selecte, we need to change the TitlesFragment
        // with the DetailsFragment. Note that the transition is not stored into the back stack, because we
        // handle fragment transitions manually. This way, when we deselect the article by setting the selected article
        // id to -1, the "TitlesFragment" will replace the detailsFragment automatically.
        articleViewModel.getSelectedArticleId().observe(this, Observer {
            if (it != -1) {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer_activity_main_portrait, DetailsFragment(), "detailsFragment")
                    .commit()
            } else {
                supportFragmentManager.findFragmentByTag("detailsFragment")?.let {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.fragmentContainer_activity_main_portrait, TitlesFragment(), "titlesFragment")
                        .commit()
                }
            }
        })
    }



    override fun onBackPressed() {
        // If the user clicks on the back button while not article is selected, we let the app exit
        if (articleViewModel.getSelectedArticleId().value == -1) {
            super.onBackPressed()
        } else {
            // If an article is selected, we deselect it. The observer declared in onCreate will automatically swipe
            // out the DetailsFragment and replace it with the TitlesFragment
            articleViewModel.selectArticle(-1)
        }
    }
}
