package com.msquaredynamics.exercise05

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.msquaredynamics.exercise05.viewmodels.ArticleViewModel
import kotlinx.android.synthetic.main.fragment_details.view.*


class DetailsFragment : Fragment() {
    private lateinit var articleViewModel: ArticleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.run {
            articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val mView = inflater.inflate(R.layout.fragment_details, container, false)

        // When we register the Observer, we'll immediately receive the last value
        articleViewModel.getSelectedArticleId().observe(this, Observer {
            if (it != -1) {
                val article = articleViewModel.getArticles().value?.find { item ->
                    item.id == it
                }

                mView.textview_frag_details_title.text = article?.title
                mView.textview_frag_details_description.text = article?.description
            }
        })

        return mView
    }
}
