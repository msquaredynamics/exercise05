package com.msquaredynamics.exercise05.utils

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View

/**
 * This class implements the ItemTouchHelper callback interface that is used by RecyclerView classes to handle drag and
 * swipe gestures.
 */
class BasicItemTouchHelperCallback(
    val adapter: ItemTouchHelperAdapter,
    private val longPressDragEnabled: Boolean = false,
    private val swipeEnabled: Boolean = false): ItemTouchHelper.Callback()
{

    override fun getMovementFlags(p0: RecyclerView, p1: RecyclerView.ViewHolder): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN

        val swipeFlags = if (adapter.isSwipeable(p1.adapterPosition)) ItemTouchHelper.END else 0
        return makeMovementFlags(dragFlags, swipeFlags)
    }


    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, targetViewHolder: RecyclerView.ViewHolder): Boolean {
        adapter.onItemMove(viewHolder.adapterPosition, targetViewHolder.adapterPosition)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        adapter.onItemDismiss(viewHolder.adapterPosition)
    }

    override fun isLongPressDragEnabled() = longPressDragEnabled
    override fun isItemViewSwipeEnabled() = swipeEnabled


    /**
     * Interface that must be implemented by the RecyclerView's adapter, used to pass the events up to the chain.
     */
    interface ItemTouchHelperAdapter {
        fun onItemMove(from: Int, to: Int)
        fun onItemDismiss(position: Int)

        /** Checks if the element at a given position can be swiped
         * @param position Element position in the adapter
         * @return True if the element can be swiped
         */
        fun isSwipeable(position: Int) : Boolean
    }

/*
    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            val itemView: View = viewHolder.itemView
            val height = itemView.bottom.toFloat() - itemView.top.toFloat()
            val width = height / 5
            viewHolder.itemView.translationX = dX/5
            val paint = Paint()
            paint.color = Color.parseColor("#D32F2F")
            val background: RectF = RectF(itemView.left.toFloat(), itemView.top.toFloat(), itemView.left.toFloat() + dX/5, itemView.bottom.toFloat())
            c.drawRect(background, paint)

        } else {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }
    }
    */
}